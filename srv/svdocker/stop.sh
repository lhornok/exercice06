#!/usr/bin/env bash

sudo docker stop symfony
sudo docker rm symfony
sudo docker build -t lhornok/symfony .
./docker_run.sh
